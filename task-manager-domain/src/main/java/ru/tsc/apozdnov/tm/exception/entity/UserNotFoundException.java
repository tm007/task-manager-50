package ru.tsc.apozdnov.tm.exception.entity;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public final class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}