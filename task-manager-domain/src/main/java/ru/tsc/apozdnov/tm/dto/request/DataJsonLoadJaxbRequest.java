package ru.tsc.apozdnov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataJsonLoadJaxbRequest extends AbstractUserRequest {

    public DataJsonLoadJaxbRequest(@Nullable String token) {
        super(token);
    }

}